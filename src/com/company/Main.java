package com.company;

import java.lang.reflect.Array;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Fast loop function Fib non use memory");
        long startTime2=System.nanoTime();
        System.out.println(fastLoopFib(50));
        Long fastloopTime= System.nanoTime()-startTime2;
        System.out.println("Time for function: "+fastloopTime+" nano second");
        System.out.println("====================================");
        System.out.println("Loop function Fib");
        long startTime1=System.nanoTime();
        ArrayList<Long> cache=new ArrayList<>();
        cache.add((long) 0.0);
        cache.add((long) 1.0);
        cache.add((long) 1.0);
        Long loopTime = (System.nanoTime()-startTime1);
        System.out.println(loopToCalculateFib(50,cache));
        System.out.println("Time for function: "+loopTime+" nano second");
        System.out.println("====================================");
        System.out.println("Recursion Fib");
        long startTime=System.nanoTime();
        System.out.println(basicRecursionFib(50));
        Long basicRecursionTime=(System.nanoTime()-startTime);
        System.out.println("Time for function: "+basicRecursionTime+" nano second");

        System.out.println("Summary:");
        System.out.println("The fast loop function faster than loop function: ~"+loopTime/fastloopTime);
        System.out.println("The loop function faster than recursion function: ~"+basicRecursionTime/loopTime);
    }

    public static long basicRecursionFib(int n) {
        if(n==1 || n==2) return 1;
        return basicRecursionFib(n-1) + basicRecursionFib(n-2);
    }

    public static long loopToCalculateFib(int n, ArrayList<Long> cache){
        if (n==1 || n==2) return 1;
        for(int index=3;index<=n;index++){
            cache.add(cache.get(index-1)+cache.get(index-2));
        }
        return cache.get(n);
    }

    public static long fastLoopFib(int n){
        if(n==1 || n==2) return 1;
        long first = (long) 1;
        long second = (long) 1;
        for (int index=3;index<=n;index++){
            second = first + second;
            first = second - first;
        }
        return second;
    }
}
