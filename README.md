Compare dynamic programming with basic

RESULT WITH N=50

Picked up JAVA_TOOL_OPTIONS: -Dfile.encoding=UTF8
Fast loop function Fib
12586269025
Time for function: 44775 nano second

Loop function Fib
12586269025
Time for function: 514564 nano second

Recursion Fib
12586269025
Time for function: 34731187353 nano second
Summary:
The fast loop function faster than loop function: ~11
The loop function faster than recursion function: ~67496

Process finished with exit code 0
